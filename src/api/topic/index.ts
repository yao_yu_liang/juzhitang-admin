import { defHttp } from '/@/utils/http/axios';
enum Api {
  tagsList = '/web/webJztTags/selectTage',
  subject = '/web/webJztClassify/subject',
  ability = '/web/webJztClassify/investigationAbility',
}

/**
 * 标签列表接口
 * @param params
 */
export const tagsList = (params) => defHttp.get({ url: Api.tagsList, params });

/**
 * 学科列表
 * @param params
 */
export const subjectList = (params) => defHttp.get({ url: Api.subject, params });

/**
 * 获取考察能力
 * @param params
 */
export const ability = (params) => defHttp.get({ url: Api.ability, params });
