import { defHttp } from '/@/utils/http/axios';
enum Api {
  grade = '/sys/dict/dictItems/jzt_grade',
  subject = '/sys/dict/dictItems/jzt_subject',
  area = '/app/provinceCityDistrict/tree',
  courseIndex = '/web/webJztCourseNode/list',
  lectorList = '/web/webJztLector/all',
  contentType = '/sys/dict/dictItems/jzt_content_type',
  topiceIndex = '/web/webJztExamTopic/solutionList',
  textbookList = '/sys/dict/dictItems/jzt_textbook_version'
}
/**
 * 教材版本列表字典
 * @param params
 */
export const getTextbookList = (params) => {
  return defHttp.get({ url: Api.textbookList, params });
};
/**
 * 年级列表字典
 * @param params
 */
export const getGradeList = (params) => {
  return defHttp.get({ url: Api.grade, params });
};

/**
 * 学科列表字典
 * @param params
 */
export const getSubjectList = (params) => {
  return defHttp.get({ url: Api.subject, params });
};

/**
 * 地区列表字典
 * @param params
 */
export const getAreaList = (params) => {
  return defHttp.get({ url: Api.area, params });
};

/**
 * 课程节点列表
 * @param params
 */
export const getCourseIndexList = (params) => {
  return defHttp.get({ url: Api.courseIndex, params });
};

/**
 * 课程节点列表
 * @param params
 */
export const getCourseLectorList = (params) => {
  return defHttp.get({ url: Api.lectorList, params });
};

/**
 * 课程节点类型字典
 * @param params
 */
export const getContentTypeList = (params) => {
  return defHttp.get({ url: Api.contentType, params });
};
/**
 * 题目答案节点
 * @param params
 */
export const getTopiceIndexList = (params) => {
  return defHttp.get({ url: Api.topiceIndex, params });
};
