import { defHttp } from '/@/utils/http/axios';
enum Api {
  grade = '/sys/dict/dictItems/jzt_agent_grade',
  status = '/sys/dict/dictItems/jzt_agent_status',
}

/**
 * 代理级别
 */
export const getGradeList = () => {
  return defHttp.get({ url: Api.grade });
};

/**
 * 代理状态
 */
export const getSubjectList = () => {
  return defHttp.get({ url: Api.status });
};
