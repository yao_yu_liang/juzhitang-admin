import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '商品信息名称',
    align:"center",
    dataIndex: 'productName'
   },
   {
    title: '奖品名称',
    align:"center",
    dataIndex: 'prizeName'
   },
   {
    title: '订单状态',
    align:"center",
    dataIndex: 'status'
   },
   {
    title: '用户名',
    align:"center",
    dataIndex: 'memberName'
   },
   {
    title: '数量',
    align:"center",
    dataIndex: 'quantity'
   },
   {
    title: '积分',
    align:"center",
    dataIndex: 'points'
   },
   {
    title: '物流公司',
    align:"center",
    dataIndex: 'company'
   },
   {
    title: '快递单号',
    align:"center",
    dataIndex: 'courierNumber'
   },
   {
    title: '合成地区',
    align:"center",
    dataIndex: 'splicingArea'
   },
   {
    title: '合成地区',
    align:"center",
    dataIndex: 'splicingAddresses'
   },
];
columns.forEach((ele)=>{
  if(ele.dataIndex === 'status'){
    ele.customRender = ({text}) => {
      let content = ''
      switch(text){
        case '01': content = '中奖'
        break
        case '02': content = '领取'
        break
        case '03': content = '邮寄'
        break
        case '04': content = '接收'
        break
        case '05': content = '退换'
        break
      }
      return content  
    }
  }
})
//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: '商品名称',
    field: 'productName',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '奖品名称',
    field: 'prizeName',
    component: 'Input',
    colProps: { span: 5 },
  },
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '商品信息名称',
    field: 'productName',
    component: 'Input',
  },
  {
    label: '奖品名称',
    field: 'prizeName',
    component: 'Input',
  },
  {
    label: '订单状态',
    field: 'status',
    component: 'InputNumber',
  },
  {
    label: '用户名',
    field: 'memberName',
    component: 'Input',
  },
  {
    label: '数量',
    field: 'quantity',
    component: 'Input',
  },
  {
    label: '积分',
    field: 'points',
    component: 'InputNumber',
  },
  {
    label: '物流公司',
    component: 'Input',
    field: 'company'
   },
   {
    label: '快递单号',
    component: 'Input',
    field: 'courierNumber'
   },
   {
    label: '合成地区',
    component: 'Input',
    field: 'splicingArea'
   },
   {
    label: '合成地区',
    component: 'Input',
    field: 'splicingAddresses'
   },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];



/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}