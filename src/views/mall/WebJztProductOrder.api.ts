import {defHttp} from '/@/utils/http/axios';
import { useMessage } from "/@/hooks/web/useMessage";

const { createConfirm } = useMessage();

enum Api {
  list = '/web/webJztProductOrder/list',
  save='/web/webJztProductOrder/add',
  edit='/web/webJztProductOrder/edit',
  deleteOne = '/web/webJztProductOrder/delete',
  deleteBatch = '/web/webJztProductOrder/deleteBatch',
  importExcel = '/web/webJztProductOrder/importExcel',
  exportXls = '/web/webJztProductOrder/exportXls',
  express = '/web/webJztProductOrderLogistics/logisticsInformation',
}
/**
 * 导出api
 * @param params
 */
export const getExportUrl = Api.exportXls;
/**
 * 导入api
 */
export const getImportUrl = Api.importExcel;
/**
 * 列表接口
 * @param params
 */
export const list = (params) =>defHttp.get({url: Api.list, params});
/**
 * 物流信息接口
 * @param params
 */
export const express = (params) => defHttp.get({ url: Api.express, params });

/**
 * 删除单个
 */
export const deleteOne = (params,handleSuccess) => {
  return defHttp.delete({url: Api.deleteOne, params}, {joinParamsToUrl: true}).then(() => {
    handleSuccess();
  });
}
/**
 * 批量删除
 * @param params
 */
export const batchDelete = (params, handleSuccess) => {
  createConfirm({
    iconType: 'warning',
    title: '确认删除',
    content: '是否删除选中数据',
    okText: '确认',
    cancelText: '取消',
    onOk: () => {
      return defHttp.delete({url: Api.deleteBatch, data: params}, {joinParamsToUrl: true}).then(() => {
        handleSuccess();
      });
    }
  });
}
/**
 * 保存或者更新
 * @param params
 */
export const saveOrUpdate = (params, isUpdate) => {
  let url = isUpdate ? Api.edit : Api.save;
  return defHttp.post({url: url, params});
}
