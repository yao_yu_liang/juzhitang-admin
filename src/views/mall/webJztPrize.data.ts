import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { Image } from 'ant-design-vue';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import { h } from 'vue';
//列表数据
export const columns: BasicColumn[] = [
  // {
  //   title: '展示图片',
  //   align: "center",
  //   dataIndex: 'ossUrl',
  //   customRender:({ record })=>{
  //     return h(Image,{
  //       src:record.image,
  //       width:135,
  //       height:47
  //     })
  //   }
  // },
  {
    title: 'name',
    align: "center",
    dataIndex: 'name'
  },
  {
    title: '权重',
    align: "center",
    dataIndex: 'weight'
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: 'name',
    field: 'name',
    component: 'Input',
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  // {
  //   label: '图片url',
  //   field: 'ossUrl',
  //   component: 'Input',
  // },
  {
    label: 'name',
    field: 'name',
    component: 'Input',
  },
  {
    label: '权重',
    field: 'weight',
    component: 'InputNumber',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
