import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import { ref, h } from 'vue';
import { Image, Switch } from 'ant-design-vue';
import { isShow } from './WebJztProduct.api';
import { useMallTypeStore } from '/@/store/modules/mall';
const mallOption = ref();
const mallStore = useMallTypeStore();
mallOption.value = mallStore.getCategory;

//列表数据
export const columns: BasicColumn[] = [

  {
    title: '商品类别 ',
    align: "center",
    dataIndex: 'productCategoryName'
  },
  {
    title: '名称',
    align: "center",
    dataIndex: 'name'
  },
  {
    title: '主图',
    align: "center",
    dataIndex: 'pic',
    customRender:({ record })=>{
      return h(Image,{
        src:record.pic,
        // width:100,
        // height:47
      })
    }
  },
  {
    title: '画册图片',
    align: "center",
    dataIndex: 'albumPics',
    customRender:({ record })=>{
      return record.albumPics.split(',').map(item=>{
        return h(Image,{
          src: item,
          // width:100,
          // height:47
        });
      });
    },
  },
  {
    title: '上架状态',
    align: "center",
    dataIndex: 'publishStatus',
    customRender:({ record })=>{
      return h(Switch,{
        checkedChildren: '是',
        unCheckedChildren: '否',
        checked: record.publishStatus === 1 ? true : false,
        // disabled: true,
         onChange: async (checked) => {
          record.publishStatus = checked ? 1 : 0;
          const params = {
            id: record.id,
            publishStatus: checked ? 1 : 0,
          };
          await isShow(params);
        },
      });
    },
  },
  // {
  //   title: '价格',
  //   align: "center",
  //   dataIndex: 'price'
  // },
  {
    title: '单位',
    align: "center",
    dataIndex: 'unit'
  },
  {
    title: '商品重量，默认为克',
    align: "center",
    dataIndex: 'weight'
  },
  {
    title: '品牌名称',
    align: "center",
    dataIndex: 'brand'
  },
  // {
  //   title: '商品分类名称',
  //   align: "center",
  //   dataIndex: 'productCategoryName'
  // },
  {
    title: '标题',
    align: "center",
    dataIndex: 'title'
  },
  {
    title: '材料',
    align: "center",
    dataIndex: 'quality'
  },

  {
    title: '详情',
    align: "center",
    dataIndex: 'details'
  },
  {
    title: '消费的积分',
    align: "center",
    dataIndex: 'points'
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: '商品类型',
    field: 'categoryId',
    component: 'Select',
    // options label 和 value 字段名可以自定义，但一定要和接口返回的字段名一致
    componentProps: {
      options: mallOption.value,
    },
  },
  {
    label: '商品名称',
    field: 'name',
    component: 'Input',
    colProps: { span: 5 },
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '商品类别 ',
    field: 'categoryId',
    component: 'InputNumber',
  },
  {
    label: '商品编码',
    field: 'outProductId',
    component: 'Input',
  },
  {
    label: 'name',
    field: 'name',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
      return [
              { required: true, message: '请输入name!'},
             ];
    },
  },
  {
    label: '主图',
    field: 'pic',
    component: 'Input',
  },
  {
    label: '画册图片，连产品图片限制为5张，以逗号分割',
    field: 'albumPics',
    component: 'Input',
  },
  {
    label: '上架状态：0->下架；1->上架',
    field: 'publishStatus',
    component: 'InputNumber',
  },
  {
    label: '排序',
    field: 'sort',
    component: 'InputNumber',
  },
  {
    label: 'price',
    field: 'price',
    component: 'InputNumber',
  },
  {
    label: '单位',
    field: 'unit',
    component: 'Input',
  },
  {
    label: '商品重量，默认为克',
    field: 'weight',
    component: 'InputNumber',
  },
  {
    label: '产品详情网页内容',
    field: 'detailHtml',
    component: 'InputTextArea',
  },
  {
    label: '移动端网页详情',
    field: 'detailMobileHtml',
    component: 'InputTextArea',
  },
  {
    label: '品牌名称',
    field: 'brand',
    component: 'Input',
  },
  {
    label: '商品分类名称',
    field: 'productCategoryName',
    component: 'Input',
  },
  {
    label: '商品销售属性，json格式',
    field: 'productAttr',
    component: 'Input',
  },
  {
    label: '标题',
    field: 'title',
    component: 'Input',
  },
  {
    label: '材料',
    field: 'quality',
    component: 'Input',
  },
  {
    label: '库存',
    field: 'inventory',
    component: 'InputNumber',
  },
  {
    label: '产地行政id',
    field: 'area',
    component: 'Input',
  },
  {
    label: '详情',
    field: 'details',
    component: 'Input',
  },
  {
    label: '使用人群 1 男 2 女 3 亲子',
    field: 'userGroup',
    component: 'Input',
  },
  {
    label: '出版社',
    field: 'publisher',
    component: 'Input',
  },
  {
    label: '作者',
    field: 'author',
    component: 'Input',
  },
  {
    label: '国籍',
    field: 'nation',
    component: 'Input',
  },
  {
    label: '消费的积分',
    field: 'points',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
