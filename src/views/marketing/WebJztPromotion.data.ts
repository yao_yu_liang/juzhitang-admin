import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import { ref, h, toRaw } from 'vue';
import { useCourseTypeStore } from '/@/store/modules/course';
const courseStore = useCourseTypeStore();
const courseGradeOption = ref();
courseGradeOption.value = courseStore.getCourseGrade;
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '增加天数',
    align: "center",
    dataIndex: 'addDay'
  },
  {
    title: '价格',
    align: "center",
    dataIndex: 'price'
  },
  {
    title: '描述',
    align: "center",
    dataIndex: 'remark'
  },
  {
    title: '创建时间',
    align: "center",
    dataIndex: 'createTime',
    customRender:({text}) =>{
      return !text?"":(text.length>10?text.substr(0,10):text);
    },
  },
  {
    title: '截至日期',
    align: "center",
    dataIndex: 'endTime',
    customRender:({text}) =>{
      return !text?"":(text.length>10?text.substr(0,10):text);
    },
  },
  {
    title: '年级',
    align: "center",
    dataIndex: 'grade_dictText',
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '增加天数',
    field: 'addDay',
    component: 'InputNumber',
  },
  {
    label: '价格',
    field: 'price',
    component: 'Input',
  },
  {
    label: '描述',
    field: 'remark',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
      return [
              { required: true, message: '请输入描述!'},
             ];
    },
  },
  {
    label: '截至日期',
    field: 'endTime',
    component: 'DatePicker',
  },
  {
    label: '年级',
    field: 'grade',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
