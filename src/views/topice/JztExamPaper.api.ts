import { defHttp } from '/@/utils/http/axios';
import { useMessage } from "/@/hooks/web/useMessage";

const { createConfirm } = useMessage();

enum Api {
  list = '/web/webJztExamPaper/list',
  save = '/web/webJztExamPaper/add',
  edit = '/web/webJztExamPaper/edit',
  deleteOne = '/web/webJztExamPaper/delete',
  deleteBatch = '/web/webJztExamPaper/deleteBatch',
  importExcel = '/web/webJztExamPaper/importExcel',
  exportXls = '/web/webJztExamPaper/exportXls',
  importPdf = '/web/webJztExamPaper/importPdf',
  isShow = '/web/webJztExamPaper/updateIsShow'
}
/**
 * 更新上架下架
 * @param params
 */
export const isShow = (params) => defHttp.get({ url: Api.isShow, params});

/**
 * 导入pdf
 * @param params
 */
export const getImportPdf = Api.importPdf;

/**
 * 导出api
 * @param params
 */
export const getExportUrl = Api.exportXls;

/**
 * 导入api
 */
export const getImportUrl = Api.importExcel;

/**
 * 列表接口
 * @param params
 */
export const list = (params) => defHttp.get({ url: Api.list, params });

/**
 * 删除单个
 * @param params
 * @param handleSuccess
 */
export const deleteOne = (params,handleSuccess) => {
  return defHttp.delete({url: Api.deleteOne, params}, {joinParamsToUrl: true}).then(() => {
    handleSuccess();
  });
}

/**
 * 批量删除
 * @param params
 * @param handleSuccess
 */
export const batchDelete = (params, handleSuccess) => {
  createConfirm({
    iconType: 'warning',
    title: '确认删除',
    content: '是否删除选中数据',
    okText: '确认',
    cancelText: '取消',
    onOk: () => {
      return defHttp.delete({url: Api.deleteBatch, data: params}, {joinParamsToUrl: true}).then(() => {
        handleSuccess();
      });
    }
  });
}

/**
 * 保存或者更新
 * @param params
 * @param isUpdate
 */
export const saveOrUpdate = (params, isUpdate) => {
  let url = isUpdate ? Api.edit : Api.save;
  return defHttp.post({ url: url, params }, { isTransformResponse: false });
}
