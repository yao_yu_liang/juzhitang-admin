import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { Cascader, Select, Image, Switch } from 'ant-design-vue';
import { h, ref } from 'vue';
import { isShow } from './WebJztExamTopic.api';
import { ability, tagsList } from '@/api/topic/index';
import { useTopicStore } from '/@/store/modules/topice';
import { useCourseTypeStore } from '/@/store/modules/course';
const courseStore = useCourseTypeStore();
const topicStore = useTopicStore();
const subjectListOption = ref();
const topicOption = ref();
const courseTypeOption = ref();
const courseGradeOption = ref();
subjectListOption.value = topicStore.getSubjectList;
topicOption.value = topicStore.getTopicTagsList;
courseTypeOption.value = courseStore.getCourseType;
courseGradeOption.value = courseStore.getCourseGrade;

//列表数据
export const columns: BasicColumn[] = [
  {
    title: '知识点',
    align: 'center',
    dataIndex: 'knowledge',
  },
  {
    title: '考察能力',
    align: "center",
    dataIndex: 'abilityId',
  },
  {
    title: '上架下架',
    align: "center",
    dataIndex: 'isShow',
    customRender: ({ record }) => {
      return h(Switch, {
        checkedChildren: '是',
        unCheckedChildren: '否',
        checked: record.isShow === '1' ? true : false,
        // disabled: true,
        onChange: async (checked) => {
          record.isShow = checked ? '1' : '0';
          console.log(record);
          const params = {
            id: record.id,
            isShow: checked ? '1' : '0'
          }
          await isShow(params)
        }
      })
    }
  },
  {
    title: '题目',
    align: "center",
    dataIndex: 'topic'
  },
  {
    title: '课程名',
    align: "center",
    dataIndex: 'courseName'
  },
  {
    title: '课程节点名称',
    align: "center",
    dataIndex: 'courseNodeName'
  },
  {
    title: '上传人',
    align: "center",
    dataIndex: 'createBy'
  },
  {
    title: '作者',
    align: "center",
    dataIndex: 'author'
  },
  {
    title: '年级',
    align: "center",
    dataIndex: 'gradeName'
  },
  {
    title: '学科',
    align: "center",
    dataIndex: 'subjectName'
  },
  {
    title: '答案',
    align: "center",
    dataIndex: 'answer'
  },
  {
    title: '题目图片',
    align: "center",
    dataIndex: 'topicPicture',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.topicPicture,
        width: 100,
        height: 47
      })
    }
  },
 
  {
    title: '选项类型',
    align: "center",
    dataIndex: 'solutionType'
  },
  {
    title: '解析',
    align: "center",
    dataIndex: 'explanation'
  },
  {
    title: '解析图片',
    align: "center",
    dataIndex: 'explanationPicture',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.explanationPicture,
        width: 100,
        height: 47
      })
    }
  },
];
columns.forEach((ele) => {
  if (ele.dataIndex === 'solutionType') {
    ele.customRender = ({ text }) => {
      let content = ''
      switch (text) {
        case '01': content = '单选'
          break
        case '02': content = '多选'
          break
      }
      return content
    }
  }
})
//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    field: 'subjectId',
    label: '学科',
    component: 'Select',
    componentProps: {
      options: subjectListOption.value,
      fieldNames: {
        label: 'subjectName',
        value: 'subjectId',
      },
    },
    colProps: { span: 5 },
  },
  {
    field: 'grade',
    label: '年级',
    component: 'Select',
    componentProps: {
      options: courseGradeOption.value,
    },
    colProps: { span: 5 },
  },
  {
    label: '题目',
    field: 'topic',
    component: 'Input',
  },
  {
    label: '课程节点名称',
    field: 'courseNodeName',
    component: 'Input',
  },
  {
    label: '上传人',
    field: 'createBy',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '解析',
    field: 'explanation',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '课程名称',
    field: 'courseName',
    component: 'Input',
  },
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '题目图片',
    field: 'topicPicture',
    component: 'JImageUpload',
    componentProps: {
      limit: 1,
      action: '/file/upload',
      placeholder: '请上传图片',
    },
  },
  {
    label: '题目',
    field: 'topic',
    component: 'Input',
  },
  // {
  //   label: '选择题类型',
  //   field: 'solutionType',
  //   component: 'Select',
  //   componentProps: {
  //     options: [
  //       { label: '单选', value: '01' },
  //       { label: '多选', value: '02' },
  //     ],
  //   },
  //   render: ({ values }) => {
  //     return h(Select, {
  //       value: values.solutionType,
  //       options: [
  //         { label: '单选', value: '01' },
  //         { label: '多选', value: '02' },
  //       ],
  //       onChange: (value) => {
  //         values.solutionType = value;
  //       },
  //       disabled: values.type !== 1,
  //     });
  //   },
  // },
  // {
  //   label: '题目类型',
  //   field: 'type',
  //   component: 'InputNumber',
  // },
  {
    label: '年级',
    field: 'grade',
    component: 'Select',
    componentProps: {
      options: courseGradeOption.value,
    },
  },
  {
    label: '学科',
    field: 'subjectId',
    component: 'Select',
    componentProps: {
      options: subjectListOption.value,
      fieldNames: {
        label: 'subjectName',
        value: 'subjectId',
      },
    },
    render: ({ model, values }) => {
      return h(Select, {
        value: model.subjectId,
        placeholder: '请选择学科',
        options: subjectListOption.value,
        fieldNames: {
          label: 'subjectName',
          value: 'subjectId',
        },
        onChange: (value) => {
          model.subjectId = value;
          model.ability = '';
          model.knowledge = [];
          model.knowledgeShow = [];
          model.tagsName = '';
          if (value) {
            ability({ subjectCode: value }).then((res) => {
              model.abilityList = res;
            });
          }
        },
      });
    },
  },
  {
    label: '考察能力',
    field: 'ability',
    component: 'Select',
    render: ({ model, values }) => {
      if (!model.abilityList && values.subjectId) {
        ability({ subjectCode: values.subjectId }).then((res) => {
          model.abilityList = res;
          model['ability'] = model['abilityId'];
          model['abilityShow'] = [];
          if (model['ability']) {
            const abilityShow = String(model['abilityId']).split(',') || [];
            abilityShow.length > 0 &&
              abilityShow.forEach((item) => {
                model['abilityShow'].push(Number(item));
              });
          }
        });
      }
      return h(Select, {
        value: model.abilityShow,
        placeholder: '请选择考察能力',
        mode: 'multiple',
        disabled: model.subjectId ? false : true,
        options: model.abilityList,
        fieldNames: {
          label: 'name',
          value: 'id',
        },
        onChange: (value) => {
          model.abilityShow = value;
          model.ability = model.abilityShow.join(',');
        },
      });
    }
  },
  {
    label: '知识点',
    field: 'knowledgeShow',
    component: 'Cascader',
    componentProps: {
      options: topicOption.value,
      //在选择框中显示搜索框,默认false
      showSearch: true,
      fieldNames: {
        label: 'tagName',
        value: 'id',
        children: 'subordinate',
      },
      multiple: true,
      onChange: (value) => {
        values.knowledgeShow = {
          knowledgeShow: value
        }
      }
    },
    //渲染 values当前表单所有值
    render: ({ model, values }) => {
      // if (typeof values.tagsName === 'string') {
      //   model.tagName = values.tagsName && values.tagsName.split(',');
      // }
      if (!model.abilityList && values.subjectId) {
        tagsList({ subjectCode: values.subjectId }).then((res) => {
          model.knowledgeList = res;
          model['knowledgeShow'] = [];
          const knowledge = model.knowledge.split(',') || [];
          knowledge.length > 0 &&
            knowledge.forEach((item) => {
              model['knowledgeShow'].push(getKnowledgeList(model.knowledgeList, item));
            });
        });
      }

      return h(Cascader, {
        value: model.knowledgeShow,
        placeholder: '请选择知识点',
        options: model.knowledgeList || topicOption.value || [],
        //在选择框中显示搜索框,默认false
        showSearch: true,
        disabled: model.subjectId ? false : true,
        fieldNames: {
          label: 'tagName',
          value: 'id',
          children: 'subordinate',
        },
        multiple: true,
        onChange: (value) => {
          // console.log('value=>',values, model,value);
          const result = value.map(subArr => subArr.slice(-1)[0]);
          model.knowledge = result;
          model.knowledgeShow = value;
        },
      });
    },
  },
  // {
  //   label: '年级',
  //   field: 'grade',
  //   component: 'Input',
  //   dynamicRules: ({model,schema}) => {
  //         return [
  //                { required: true, message: '请输入年级!'},
  //         ];
  //    },
  // },
  // {
  //   label: '配分',
  //   field: 'partition',
  //   component: 'InputNumber',
  //   dynamicRules: ({model,schema}) => {
  //         return [
  //                { required: true, message: '请输入配分!'},
  //         ];
  //    },
  // },
  {
    label: '是否删除',
    field: 'isDelete',
    component: 'Input',
    // dynamicRules: ({model,schema}) => {
    //       return [
    //              { required: true, message: '请输入是否删除!'},
    //       ];
    //  },
  },
  {
    label: '难度',
    field: 'degree',
    component: 'Rate',
    componentProps: {
      allowHalf: true,
      count: 5,
    },
  },
  {
    label: '选项类型',
    field: 'solutionType',
    component: 'Select',
    componentProps: {
      options: [
        { label: '单选', value: '01' },
        { label: '多选', value: '02' },
      ],
    },
  },
  {
    label: '答案',
    field: 'answer',
    component: 'Input',
  },
  {
    label: '解析',
    field: 'explanation',
    component: 'Input',
  },
  // TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false
  },
  {
    label: '',
    field: 'knowledge',
    component: 'Input',
    show: false,
  },
  {
    label: '',
    field: 'abilityId',
    component: 'Input',
    show: false,
  },
];



/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[] {
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}

function getKnowledgeList(ids: any, id) {
  let list = [];
  let i = 0;
  let dataList = [];
  function getChild(datas, index) {
    for (var key = 0; key < datas.length; key++) {
      if (datas[key].id == id) {
        dataList[index] = datas[key].id;
        dataList.splice(index + 1);
        list.push(...dataList);
        dataList = [];
        break;
      } else if (datas[key].subordinate.length > 0) {
        dataList[index] = datas[key].id;
        getChild(datas[key].subordinate, index + 1);
      }
    }
  }
  getChild(ids, i);
  return list;
}
