import { defHttp } from '/@/utils/http/axios';
import { useMessage } from '/@/hooks/web/useMessage';

const { createConfirm } = useMessage();

enum Api {
  list = '/web/webJztExamPaperTopic/list',
  save = '/web/webJztExamPaperTopic/add',
  edit = '/web/webJztExamPaperTopic/edit',
  deleteOne = '/web/webJztExamPaperTopic/delete',
  deleteBatch = '/web/webJztExamPaperTopic/deleteBatch',
  importExcel = '/web/webJztExamPaperTopic/importExcel',
  exportXls = '/web/webJztExamPaperTopic/exportXls',
  updateIsShow = '/web/webJztExamPaperTopic/updateIsShow',
  editBatchS = '/web/webJztExamPaperTopic/putAwayBatch',
  editBatchX = '/web/webJztExamPaperTopic/outBatch',
  getAbility = '/web/webJztClassify/investigationAbility',
  getSelectTage = 'web/webJztTags/selectTage',
}
/**
 * 更新上架下架
 * @param params
 */
export const isShow = (params) => defHttp.get({ url: Api.updateIsShow, params });

/**
 * 获取考察能力
 * @param params
 */
export const getAbility = (params) => defHttp.get({ url: Api.getAbility, params });

/**
 * 获取考察能力
 * @param params
 */
export const getSelectTage = (params) => defHttp.get({ url: Api.getSelectTage, params });

/**
 * 导出api
 * @param params
 */
export const getExportUrl = Api.exportXls;
/**
 * 导入api
 */
export const getImportUrl = Api.importExcel;
/**
 * 列表接口
 * @param params
 */
export const list = (params) => defHttp.get({ url: Api.list, params });

/**
 * 删除单个
 */
export const deleteOne = (params, handleSuccess) => {
  return defHttp.delete({ url: Api.deleteOne, params }, { joinParamsToUrl: true }).then(() => {
    handleSuccess();
  });
};
/**
 * 批量删除
 * @param params
 */
export const batchDelete = (params, handleSuccess) => {
  createConfirm({
    iconType: 'warning',
    title: '确认删除',
    content: '是否删除选中数据',
    okText: '确认',
    cancelText: '取消',
    onOk: () => {
      return defHttp.delete({ url: Api.deleteBatch, data: params }, { joinParamsToUrl: true }).then(() => {
        handleSuccess();
      });
    },
  });
};
/**
 * 批量上架
 * @param params
 */
// export const editBatch = (params) => defHttp.get({ url: Api.editBatch, params});
export const editBatchS = (params, handleSuccess) => {
  createConfirm({
    iconType: 'warning',
    title: '确认上架',
    content: '是否确认上架选中数据',
    okText: '确认',
    cancelText: '取消',
    onOk: () => {
      return defHttp.post({ url: Api.editBatchS, data: params }, { joinParamsToUrl: true }).then(() => {
        handleSuccess();
      });
    },
  });
};
/**
 * 批量下架
 * @param params
 */
// export const editBatch = (params) => defHttp.get({ url: Api.editBatch, params});
export const editBatchX = (params, handleSuccess) => {
  createConfirm({
    iconType: 'warning',
    title: '确认下架',
    content: '是否确认下架选中数据',
    okText: '确认',
    cancelText: '取消',
    onOk: () => {
      return defHttp.post({ url: Api.editBatchX, data: params }, { joinParamsToUrl: true }).then(() => {
        handleSuccess();
      });
    },
  });
};
/**
 * 保存或者更新
 * @param params
 */
export const saveOrUpdate = (params, isUpdate) => {
  const url = isUpdate ? Api.edit : Api.save;
  return defHttp.post({ url: url, params });
};
