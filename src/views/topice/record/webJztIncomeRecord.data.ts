import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { useCourseTypeStore } from '/@/store/modules/course';
import { isShow } from './webJztIncomeRecord.api';
import { useTopicStore } from '/@/store/modules/topice';
import { Cascader, Select, Image, Switch, Upload } from 'ant-design-vue';
import { JUpload } from '/@/components/Form/src/jeecg/components/JUpload';
import { ref, h, toRaw } from 'vue';
import { ability, tagsList } from '@/api/topic/index';
const courseStore = useCourseTypeStore();
const topicStore = useTopicStore();
const courseTypeOption = ref();
const courseGradeOption = ref();
const topicOption = ref();
const subjectListOption = ref();
const areaOption = ref();
const textBookOption = ref();
courseTypeOption.value = courseStore.getCourseType;
courseGradeOption.value = courseStore.getCourseGrade;
areaOption.value = courseStore.getArea;
textBookOption.value = courseStore.getTextBook;
topicOption.value = topicStore.getTopicTagsList;
subjectListOption.value = topicStore.getSubjectList;
const videoUrl = ref();
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '知识点',
    align: 'center',
    dataIndex: 'knowledge',
  },
  {
    title: '考察能力',
    align: "center",
    dataIndex: 'abilityId',
  },
  {
    title: '上架下架',
    align: "center",
    dataIndex: 'isShow',
    customRender: ({ record }) => {
      return h(Switch, {
        checkedChildren: '是',
        unCheckedChildren: '否',
        checked: record.isShow === '1' ? true : false,
        // disabled: true,
        onChange: async (checked) => {
          record.isShow = checked ? '1' : '0';
          // console.log(record);
          const params = {
            id: record.id,
            isShow: checked ? '1' : '0',
          };
          await isShow(params);
        },
      });
    },
  },
  {
    title: '课程名',
    align: "center",
    dataIndex: 'courseName'
  },
  {
    title: '课程节点名称',
    align: "center",
    dataIndex: 'courseNodeName'
  },
  {
    title: '题目',
    align: "center",
    dataIndex: 'topic'
  },
  {
    title: '题型', // 1 选择题 2 填空 3 解答题
    align: "center",
    dataIndex: 'type',
    customRender: ({ record }) => {
      return record.type === 1 ? '选择题' : record.type === 2 ? '填空' : '解答题';
    },
  },
  {
    title: '上传人',
    align: "center",
    dataIndex: 'createBy'
  },
  {
    title: '作者',
    align: "center",
    dataIndex: 'author'
  },
  {
    title: '年级',
    align: "center",
    dataIndex: 'gradeName',
  },
  {
    title: '题目图片',
    align: "center",
    dataIndex: 'topicPicture',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.topicPicture,
        width: 100,
        height: 47
      })
    }
  },
  {
    title: '答案',
    align: "center",
    dataIndex: 'answer'
  },
  {
    title: '答案解析',
    align: "center",
    dataIndex: 'explanation'
  },
  {
    title: '解析图片',
    align: "center",
    dataIndex: 'explanationPicture',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.explanationPicture,
        width: 100,
        height: 47
      })
    }
  },
  {
    title: '学科',
    align: "center",
    dataIndex: 'subjectName'
  },
  {
    title: '地区（省级）',
    align: "center",
    dataIndex: 'areaId'
  },
  {
    title: '难度',
    align: "center",
    dataIndex: 'degree'
  },
  {
    title: '考察能力',
    align: "center",
    dataIndex: 'abilityName'
  },

  {
    title: '视频url',
    align: "center",
    dataIndex: 'videoUrl'
  },
  {
    title: '视频标题',
    align: "center",
    dataIndex: 'videoTitle'
  },
  {
    title: '视频封面',
    align: "center",
    dataIndex: 'videoImage'
  },
  {
    title: '知识点',
    align: "center",
    dataIndex: 'knowledge'
  },
  {
    title: '标签',
    align: "center",
    dataIndex: 'tagsName'
  },
  
];
//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    field: 'subjectId',
    label: '学科',
    component: 'Select',
    componentProps: {
      options: subjectListOption.value,
      fieldNames: {
        label: 'subjectName',
        value: 'subjectId',
      },
    },
    colProps: { span: 5 },
  },

  {
    field: 'grade',
    label: '年级',
    component: 'Select',
    componentProps: {
      options: courseGradeOption.value,
    },
    colProps: { span: 5 },
  },
  {
    label: '题目',
    field: 'topic',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '课程节点名称',
    field: 'courseNodeName',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '上传人',
    field: 'createBy',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '解析',
    field: 'explanation',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '课程名称',
    field: 'courseName',
    component: 'Input',
    colProps: { span: 5 },
  },


];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '题目',
    field: 'topic',
    component: 'Input',
  },
  {
    label: '学科',
    field: 'subjectId',
    component: 'Select',
    componentProps: {
      options: subjectListOption.value,
      fieldNames: {
        label: 'subjectName',
        value: 'subjectId',
      },
    },
    render: ({ model, values }) => {
      return h(Select, {
        value: model.subjectId,
        placeholder: '请选择学科',
        options: subjectListOption.value,
        fieldNames: {
          label: 'subjectName',
          value: 'subjectId',
        },
        onChange: (value) => {
          model.subjectId = value;
          model.ability = '';
          model.knowledge = [];
          model.knowledgeShow = [];
          model.tagsName = '';
          if (value) {
            ability({ subjectCode: value }).then((res) => {
              model.abilityList = res;
            });
          }
        },
      });
    },
  },
  {
    label: '考察能力',
    field: 'ability',
    component: 'Select',
    render: ({ model, values }) => {
      if (!model.abilityList && values.subjectId) {
        ability({ subjectCode: values.subjectId }).then((res) => {
          model.abilityList = res;
          model['ability'] = model['abilityId'];
          model['abilityShow'] = [];
          if (model['ability']) {
            const abilityShow = String(model['abilityId']).split(',') || [];
            abilityShow.length > 0 &&
              abilityShow.forEach((item) => {
                model['abilityShow'].push(Number(item));
              });
          }
        });
      }
      return h(Select, {
        value: model.abilityShow,
        placeholder: '请选择考察能力',
        mode: 'multiple',
        disabled: model.subjectId ? false : true,
        options: model.abilityList,
        fieldNames: {
          label: 'name',
          value: 'id',
        },
        onChange: (value) => {
          model.abilityShow = value;
          model.ability = model.abilityShow.join(',');
        },
      });
    }
  },
  {
    label: '知识点',
    field: 'knowledgeShow',
    component: 'Cascader',
    componentProps: {
      options: topicOption.value,
      //在选择框中显示搜索框,默认false
      showSearch: true,
      fieldNames: {
        label: 'tagName',
        value: 'id',
        children: 'subordinate',
      },
      multiple: true,
      onChange: (value) => {
        values.knowledgeShow = {
          knowledgeShow: value
        }
      }
    },
    //渲染 values当前表单所有值
    render: ({ model, values }) => {
      // if (typeof values.tagsName === 'string') {
      //   model.tagName = values.tagsName && values.tagsName.split(',');
      // }
      if (!model.abilityList && values.subjectId) {
        tagsList({ subjectCode: values.subjectId }).then((res) => {
          model.knowledgeList = res;
          model['knowledgeShow'] = [];
          const knowledge = model.knowledge.split(',') || [];
          knowledge.length > 0 &&
            knowledge.forEach((item) => {
              model['knowledgeShow'].push(getKnowledgeList(model.knowledgeList, item));
            });
        });
      }

      return h(Cascader, {
        value: model.knowledgeShow,
        placeholder: '请选择知识点',
        options: model.knowledgeList || topicOption.value || [],
        //在选择框中显示搜索框,默认false
        showSearch: true,
        disabled: model.subjectId ? false : true,
        fieldNames: {
          label: 'tagName',
          value: 'id',
          children: 'subordinate',
        },
        multiple: true,
        onChange: (value) => {
          // console.log('value=>',values, model,value);
          const result = value.map(subArr => subArr.slice(-1)[0]);
          model.knowledge = result;
          model.knowledgeShow = value;
        },
      });
    },
  },
  // {
  //   label: '标签',
  //   field: 'tagsName',
  //   component: 'Input',
  // },
  {
    label: '题型',
    field: 'type',
    component: 'Select',
    componentProps: {
      options: [
        { label: '选择题', value: 1 },
        { label: '填空', value: 2 },
        { label: '解答题', value: 3 },
      ],
    },
    dynamicRules: ({ model, schema }) => {
      return [{ required: true, message: '请选择题目类型' }];
    },
  },
  {
    label: '选择题类型',
    field: 'solutionType',
    component: 'Select',
    componentProps: {
      options: [
        { label: '单选', value: '01' },
        { label: '多选', value: '02' },
      ],
    },
    render: ({ values }) => {
      return h(Select, {
        value: values.solutionType,
        options: [
          { label: '单选', value: '01' },
          { label: '多选', value: '02' },
        ],
        onChange: (value) => {
          values.solutionType = value;
        },
        disabled: values.type !== 1,
      });
    },
  },
  {
    label: '年级',
    field: 'grade',
    component: 'Select',
    componentProps: {
      options: courseGradeOption.value,
    },
  },
  {
    label: '教材版本',
    field: 'textbookVersion',
    component: 'Select',
    componentProps: {
      options: textBookOption.value,
    },
  },
  {
    label: '答案',
    field: 'answer',
    component: 'Input',
  },
  {
    label: '解析',
    field: 'explanation',
    component: 'Input',
  },

  {
    label: '题目图片',
    field: 'topicPicture',
    component: 'JImageUpload',
    componentProps: {
      limit: 1,
      action: '/file/upload',
      placeholder: '请上传图片',
    },
  },
  {
    label: '地区（省级）',
    field: 'areaId',
    component: 'Select',
    componentProps: {
      options: areaOption.value,
      fieldNames: {
        label: 'name',
        value: 'id',
      },
    },
  },
  {
    label: '难度',
    field: 'degree',
    component: 'Rate',
    componentProps: {
      allowHalf: true,
      count: 5,
    },
  },
  {
    label: '上传视频',
    field: 'videoUrl',
    component: 'JUpload',
    render: ({ model, values }) => {
      return h(JUpload, {
        value: model.videoUrl,
        limit: 1,
        action: '/sys/initiateMultipartUpload/uploadOss',
        listType: 'text',
        multiple: false,
        accept: 'video/*',
        onSuccess: (info) => {
          // console.log('value', info);
          model.videoUrl = info;
        },
      });
    },
  },
  {
    label: '视频url',
    field: 'videoUrl',
    component: 'Input',
    componentProps: {
      disabled: true,
      value: videoUrl.value,
    },
  },
  {
    label: '视频标题',
    field: 'videoTitle',
    component: 'Input',
  },
  // {
  //   label: '视频上传',
  //   field: 'videoUrl',
  //   component: 'JUpload',
  //   componentProps: {
  //     limit: 1,
  //     placeholder: '请上传视频',
  //   },
  // },
  {
    label: '视频封面',
    field: 'videoImage',
    component: 'JImageUpload',
    componentProps: {
      limit: 1,
      action: '/file/upload',
      placeholder: '请上传图片',
    },
  },
  {
    label: '答案解析图片',
    field: 'explanationPicture',
    component: 'JImageUpload',
    componentProps: {
      limit: 1,
      action: '/file/upload',
      placeholder: '请上传图片',
    },
  },
  {
    label: '是否推荐',
    field: 'isRecommend',
    component: 'Select',
    componentProps: {
      options: [
        { label: '是', value: '1' },
        { label: '否', value: '0' },
      ],
    },
  },
  // TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
  {
    label: '',
    field: 'knowledge',
    component: 'Input',
    show: false,
  },
  {
    label: '',
    field: 'abilityId',
    component: 'Input',
    show: false,
  },
];

/**
 * 流程表单调用这个方法获取formSchema
 * @param param
 */
export function getBpmFormSchema(_formData): FormSchema[] {
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}

function getKnowledgeList(ids: any, id) {
  let list = [];
  let i = 0;
  let dataList = [];
  function getChild(datas, index) {
    for (var key = 0; key < datas.length; key++) {
      if (datas[key].id == id) {
        dataList[index] = datas[key].id;
        dataList.splice(index + 1);
        list.push(...dataList);
        dataList = [];
        break;
      } else if (datas[key].subordinate.length > 0) {
        dataList[index] = datas[key].id;
        getChild(datas[key].subordinate, index + 1);
      }
    }
  }
  getChild(ids, i);
  return list;
}
