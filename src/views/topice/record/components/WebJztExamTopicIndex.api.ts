import { defHttp } from '/@/utils/http/axios';

enum Api {
  courseLiverUrl = '/web/webJztCourseNode/liveUrl',
  indexAdd = '/web/webJztCourseNode/add',
  indexEdit = '/web/webJztExamTopic/editSolution',
  indexDel = '/web/webJztCourseNode/delete',
  tagsSave = '/web/webJztExamTopic/setTags',
  tagsDel = '/web/webJztTagsTopic/deleteBytags',
}

/**
 * 生成直播地址
 * @param params
 */
export const getCourseLiverUrl = (params) => {
  return defHttp.get({ url: Api.courseLiverUrl, params }, { successMessageMode: 'none' });
};

/**
 * 课程节点新增 / 修改
 * @param params
 */
export const saveOrUpdate = (params, isUpdate) => {
  const url = isUpdate ? Api.indexEdit : Api.indexAdd;
  return defHttp.post({ url: url, params }, { isTransformResponse: false });
};

/**
 * 删除
 * @param params
 */
export const indexDel = (params) => {
  return defHttp.delete({ url: Api.indexDel + '?id=' + params.id });
};

/**
 * 标签新增接口
 * @param params
 */
export const tagsSave = (params, handleSuccess) => {
  return defHttp.post({ url: Api.tagsSave + '?tagsIds=' + params.tagsIds + '&topicIds=' + params.topicIds }).then(() => {
    handleSuccess();
  });
};

/**
 * 标签删除接口
 * @param params
 */
export const tagsDel = (params, handleSuccess) => {
  return defHttp.delete({ url: Api.tagsDel + '?tagsIds=' + params.tagsIds + '&topicIds=' + params.topicIds }).then(() => {
    handleSuccess();
  });
};
