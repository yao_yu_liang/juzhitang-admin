import { BasicColumn } from '/@/components/Table';
import { Image } from 'ant-design-vue';
import { h } from 'vue';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '选项',
    align: 'center',
    dataIndex: 'prefs',
  },
  {
    title: '选项内容',
    align: "center",
    dataIndex: 'solution',
    customRender:({ record })=>{
      if(record.isPicture === '1'){
        return h(Image,{
        src:record.solution,
        width:135,
        height:47
      })
      }else{
        return h('span',null,record.solution)
      }   
    }
  },
  {
    title: '操作',
    align: 'center',
    key: 'operation',
    width: 220,
    slots: { customRender: 'operation' },
  },
];
