import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import { useCourseTypeStore } from '/@/store/modules/course';
import { isShow } from './JztExamPaper.api';
import { ref } from 'vue';
import { h } from 'vue';
import { Image,Switch } from 'ant-design-vue';
const courseStore = useCourseTypeStore();
const courseTypeOption = ref();
const courseGradeOption = ref();
courseTypeOption.value = courseStore.getCourseType;
courseGradeOption.value = courseStore.getCourseGrade;
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '试卷标题',
    align: "center",
    dataIndex: 'title'
  },
  {
    title: '下载路径',
    align: "center",
    dataIndex: 'ossUrl'
  },
  {
    title: '难度',
    align: "center",
    dataIndex: 'degree'
  },
  {
    title: '地区',
    align: "center",
    dataIndex: 'area'
  },
  {
    title: '学科',
    align: "center",
    dataIndex: 'subjectId'
  },
  {
    title: '年级',
    align: "center",
    dataIndex: 'grade'
  },
  {
    title: '消费积分',
    align: "center",
    dataIndex: 'points'
  },
  {
    title: '试卷头部绿色标签',
    align: "center",
    dataIndex: 'tags'
  },
  {
    title: '年份',
    align: "center",
    dataIndex: 'year'
  },
  {
    title: 'pdf预览图',
    align: "center",
    dataIndex: 'preview',
    customRender:({ record })=>{
      return h(Image,{
        src:record.preview,
        width:135,
        height:47
      })
    }
  },
  {
    title: '上架下架',
    align: "center",
    dataIndex: 'isShow',
    customRender:({ record })=>{
      return h(Switch,{
        checkedChildren: '是',
        unCheckedChildren: '否',
        checked: record.isShow === '1' ? true : false,
        // disabled: true,
         onChange: async (checked) => {
          record.isShow = checked ? '1' : '0';
          console.log(record);
          const params = {
            id: record.id,
            isShow:checked ? '1' : '0'
          }
          await isShow(params)
        }
      })
    }
  },
];
// columns.forEach((ele) => {
//   if(ele.dataIndex === 'isShow'){
//     ele.customRender = ({ text }) => {
//       let content = '';
//       switch(text) {
//         case '1': content = '显示'
//         break;
//         case '0': content = '隐藏'
//         break;
//       }
//       return content;
//     };
//   }
// });

//查询数据
export const searchFormSchema: FormSchema[] = [
  /**
   *  title 标题
      atea 地区
      subjectId 学科
      grade 年级
      year 年份
   * **/ 
  {
    label: '试卷标题',
    field: 'title',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '地区',
    field: 'area',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '学科',
    field: 'subjectId',
    component: 'Select',
    componentProps: {
      options: courseTypeOption.value,
    },
    colProps: { span: 5 },
  },
  {
    label: '年级',
    field: 'grade',
    component: 'Select',
    componentProps: {
      options: courseGradeOption.value,
    },
    colProps: { span: 5 },
  },
  {
    label: '年份',
    field: 'year',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '是否展示',
    field: 'isShow',
    component: 'Select',
    componentProps: {
      options: [
        {value:'0',label:'隐藏'},
        {value:'1',label:'展示'},
      ],
    },
    colProps: { span: 5 },
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '试卷标题',
    field: 'title',
    component: 'Input',
  },
  {
    label: '下载路径',
    field: 'ossUrl',
    component: 'Input',
  },
  {
    label: '难度',
    field: 'degree',
    component: 'Input',
  },
  {
    label: '地区',
    field: 'area',
    component: 'Input',
  },
  {
    label: '学科',
    field: 'subjectId',
    component: 'Input',
  },
  {
    label: '年级',
    field: 'grade',
    component: 'Input',
  },
  {
    label: '消费积分',
    field: 'points',
    component: 'InputNumber',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
