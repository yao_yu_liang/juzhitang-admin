import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { Image } from 'ant-design-vue';

import { h } from 'vue';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '昵称',
    align:"center",
    dataIndex: 'name'
  },
  {
    title: '教师头像',
    align: 'center',
    dataIndex: 'image',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.image,
        width: 30,
        height: 30,
      });
    },
  },
  {
    title: '手机号码',
    align:"center",
    dataIndex: 'phone'
  },
  {
    title: '推荐',
    align:"center",
    dataIndex: 'isRecommend',
    customRender:({text}) =>{
      return text == 1 ? '是' : text == 0 ? '否' : '';
    },
  },
  {
    title: '对应的用户ID',
    align:"center",
    dataIndex: 'memberId'
  },
  {
    title: '主讲内容',
    align:"center",
    dataIndex: 'content'
  },
  {
    title: '教师简介',
    align:"center",
    dataIndex: 'note'
  },
  {
    title: '身份证号码',
    align:"center",
    dataIndex: 'cardNumber'
  },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '昵称',
    field: 'name',
    component: 'Input',
  },
  {
    field: 'image',
    component: 'JImageUpload',
    label: '头像上传',
    helpMessage: '最多上传1张图片',
    componentProps: {
      fileMax: 1,
    },
  },
  {
    label: '手机号码',
    field: 'phone',
    component: 'Input',
  },
  {
    label: '标签',
    field: 'tags',
    component: 'Input',
  },
  {
    label: '教师简介',
    field: 'details',
    component: 'JEditor',
  },
  {
    label: '对应的用户ID',
    field: 'memberId',
    component: 'InputNumber',
  },
  {
    label: '主讲内容',
    field: 'content',
    component: 'Input',
  },
  {
    label: '教师简介',
    field: 'note',
    component: 'Input',
  },
  {
    label: '身份证号码',
    field: 'cardNumber',
    component: 'Input',
  },
  {
    label: '推荐 0 否 1是',
    field: 'isRecommend',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];



/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}
