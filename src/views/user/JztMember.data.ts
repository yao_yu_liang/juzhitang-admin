import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
   {
    title: '登录账号',
    align:"center",
    dataIndex: 'username'
   },
   {
    title: '真实姓名',
    align:"center",
    dataIndex: 'realname'
   },
   {
    title: '密码',
    align:"center",
    dataIndex: 'password'
   },
   {
    title: '生日',
    align:"center",
    dataIndex: 'birthday'
   },
   {
    title: '身份（1游客 2老师）',
    align:"center",
    dataIndex: 'userIdentity'
   },
   {
    title: '联系电话',
    align:"center",
    dataIndex: 'phone'
   },
   {
    title: '性别',
    align:"center",
    dataIndex: 'sex'
   },
   {
    title: '头像',
    align:"center",
    dataIndex: 'avatar'
   },
   {
    title: '地区',
    align:"center",
    dataIndex: 'area'
   },
   {
    title: '推荐人id',
    align:"center",
    dataIndex: 'recommend'
   },
   {
    title: '0 否 1是',
    align:"center",
    dataIndex: 'isDel'
   },
   {
    title: '密码盐',
    align:"center",
    dataIndex: 'salt'
   },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '登录账号',
    field: 'username',
    component: 'Input',
  },
  {
    label: '真实姓名',
    field: 'realname',
    component: 'Input',
  },
  {
    label: '密码',
    field: 'password',
    component: 'Input',
  },
  {
    label: '生日',
    field: 'birthday',
    component: 'Input',
  },
  {
    label: '身份（1游客 2老师）',
    field: 'userIdentity',
    component: 'Input',
  },
  {
    label: '联系电话',
    field: 'phone',
    component: 'Input',
  },
  {
    label: '性别',
    field: 'sex',
    component: 'Input',
  },
  {
    label: '头像',
    field: 'avatar',
    component: 'Input',
  },
  {
    label: '地区',
    field: 'area',
    component: 'Input',
  },
  {
    label: '推荐人id',
    field: 'recommend',
    component: 'InputNumber',
  },
  {
    label: '0 否 1是',
    field: 'isDel',
    component: 'Input',
  },
  {
    label: '密码盐',
    field: 'salt',
    component: 'Input',
  },
  {
    label: '用户id',
    field: 'memberId',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];



/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}