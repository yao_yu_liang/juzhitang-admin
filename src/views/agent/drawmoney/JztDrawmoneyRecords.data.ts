import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
//列表数据
export const columns: BasicColumn[] = [
  // {
  //   title: 'id',
  //   align: "center",
  //   dataIndex: 'memberId'
  // },
  {
    title: '用户名称',
    align: "center",
    dataIndex: 'username'
  },
  {
    title: '提现时间起',
    align: "center",
    dataIndex: 'startTime',
    customRender:({text}) =>{
      return !text?"":(text.length>10?text.substr(0,10):text);
    },
  },
  {
    title: '截至时间',
    align: "center",
    dataIndex: 'endTime',
    customRender:({text}) =>{
      return !text?"":(text.length>10?text.substr(0,10):text);
    },
  },
  {
    title: '提现金额',
    align: "center",
    dataIndex: 'amount'
  },
  {
    title: '提现状态', // 01 申请 02 提现完成
    align: "center",
    dataIndex: 'statusName',
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
];

//表单数据
export const formSchema: FormSchema[] = [
  // {
  //   label: 'memberId',
  //   field: 'memberId',
  //   component: 'InputNumber',
  // },
  {
    label: '提现时间起',
    field: 'startTime',
    component: 'DatePicker',
  },
  {
    label: '截至时间',
    field: 'endTime',
    component: 'DatePicker',
  },
  {
    label: '提现金额',
    field: 'amount',
    component: 'InputNumber',
  },
  {
    label: '提现状态 01 申请 02 提现完成',
    field: 'status',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
