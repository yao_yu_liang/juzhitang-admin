import { BasicColumn } from '/@/components/Table';
import { Image } from 'ant-design-vue';
import { h } from 'vue';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '用户id',
    align:"center",
    dataIndex: 'memberId'
   },
   {
    title: '用户名',
    align:"center",
    dataIndex: 'memberName'
   },
   {
    title: '身份证',
    align:"center",
    dataIndex: 'identityCard'
   },
   {
    title: '手机号',
    align:"center",
    dataIndex: 'phone'
   },
  {
    title: '操作',
    align: 'center',
    key: 'operation',
    width: 220,
    slots: { customRender: 'operation' },
  },
];
