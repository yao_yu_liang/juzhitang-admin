import {defHttp} from '/@/utils/http/axios';
import { useMessage } from "/@/hooks/web/useMessage";

const { createConfirm } = useMessage();

enum Api {
  list ='/web/webJztDistributionAgency/one',
  listIndex = '/web/webJztDistributionAgency/list',
  save='/web/webJztDistributionAgency/add',
  edit='/web/webJztDistributionAgency/edit',
  deleteOne = '/web/webJztDistributionAgency/delete',
  deleteBatch = '/web/webJztDistributionAgency/deleteBatch',
  importExcel = '/web/webJztDistributionAgency/importExcel',
  exportXls = '/web/webJztDistributionAgency/exportXls',
  orderList = '/web/webJztDistributionAgency/orderPageList',
  orderIndexList = '/web/webJztDistributionAgency/orderList'
}
/**
 * 导出api
 * @param params
 */
export const getExportUrl = Api.exportXls;
/**
 * 导入api
 */
export const getImportUrl = Api.importExcel;
/**
 * 列表接口
 * @param params
 */
export const list = (params) =>
  defHttp.get({url: Api.list, params});

/**
 * 列表子节点接口
 * @param params
 */
export const listIndex = (params) =>
  defHttp.get({url: Api.listIndex, params});

/**
 * 订单列表接口
 * @param params
 */
export const orderList = (params) =>
  defHttp.get({url: Api.orderList, params});

/**
 * 订单子节点列表接口
 * @param params
 */
export const orderIndexList = (params) =>
  defHttp.get({url: Api.orderIndexList, params});

/**
 * 删除单个
 */
export const deleteOne = (params,handleSuccess) => {
  return defHttp.delete({url: Api.deleteOne, params}, {joinParamsToUrl: true}).then(() => {
    handleSuccess();
  });
}
/**
 * 批量删除
 * @param params
 */
export const batchDelete = (params, handleSuccess) => {
  createConfirm({
    iconType: 'warning',
    title: '确认删除',
    content: '是否删除选中数据',
    okText: '确认',
    cancelText: '取消',
    onOk: () => {
      return defHttp.delete({url: Api.deleteBatch, data: params}, {joinParamsToUrl: true}).then(() => {
        handleSuccess();
      });
    }
  });
}
/**
 * 保存或者更新
 * @param params
 */
export const saveOrUpdate = (params, isUpdate) => {
  let url = isUpdate ? Api.edit : Api.save;
  return defHttp.post({url: url, params});
}
