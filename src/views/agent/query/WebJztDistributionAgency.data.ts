import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import { list } from './WebJztDistributionAgency.api';
import { ref, h } from 'vue';
import { Input } from 'ant-design-vue';
import { func } from 'vue-types';
//列表数据
export const columns: BasicColumn[] = [
   {
    title: '上级代理id',
    align:"center",
    dataIndex: 'superiorId'
   },
   {
    title: '用户id',
    align:"center",
    dataIndex: 'memberId'
   },
   {
    title: '用户名',
    align:"center",
    dataIndex: 'memberName'
   },
   {
    title: '身份证',
    align:"center",
    dataIndex: 'identityCard'
   },
   {
    title: '手机号',
    align:"center",
    dataIndex: 'phone'
   },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: '用户id',
    field: 'memberId',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '用户名',
    field: 'memberName',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '身份证',
    field: 'identityCard',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '手机号',
    field: 'phone',
    component: 'Input',
    colProps: { span: 5 },
  },
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '上级代理人id',
    field: 'superiorId',
    component: 'InputNumber',
    componentProps: {
      disabled: true,
    },
  },
  {
    label: '用户id',
    field: 'memberId',
    component: 'Input',
  },
  {
    label: '用户名',
    field: 'memberName',
    component: 'Input',
  },
  {
    label: '身份证',
    field: 'identityCard',
    component: 'Input',
  },
  {
    label: '手机号',
    field: 'phone',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];



/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}