-- 注意：该页面对应的前台目录为views/web文件夹下
-- 如果你想更改到其他目录，请修改sql中component字段对应的值


INSERT INTO sys_permission(id, parent_id, name, url, component, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_route, is_leaf, keep_alive, hidden, hide_tab, description, status, del_flag, rule_flag, create_by, create_time, update_by, update_time, internal_or_external) 
VALUES ('2024061203219140510', NULL, 'jzt_distribution_agency', '/web/webJztDistributionAgencyList', 'web/WebJztDistributionAgencyList', NULL, NULL, 0, NULL, '1', 0.00, 0, NULL, 1, 1, 0, 0, 0, NULL, '1', 0, 0, 'admin', '2024-06-12 15:21:51', NULL, NULL, 0);

-- 权限控制sql
-- 新增
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061203219140511', '2024061203219140510', '添加jzt_distribution_agency', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_distribution_agency:add', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 15:21:51', NULL, NULL, 0, 0, '1', 0);
-- 编辑
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061203219140512', '2024061203219140510', '编辑jzt_distribution_agency', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_distribution_agency:edit', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 15:21:51', NULL, NULL, 0, 0, '1', 0);
-- 删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061203219140513', '2024061203219140510', '删除jzt_distribution_agency', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_distribution_agency:delete', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 15:21:51', NULL, NULL, 0, 0, '1', 0);
-- 批量删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061203219140514', '2024061203219140510', '批量删除jzt_distribution_agency', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_distribution_agency:deleteBatch', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 15:21:51', NULL, NULL, 0, 0, '1', 0);
-- 导出excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061203219140515', '2024061203219140510', '导出excel_jzt_distribution_agency', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_distribution_agency:exportXls', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 15:21:51', NULL, NULL, 0, 0, '1', 0);
-- 导入excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061203219140516', '2024061203219140510', '导入excel_jzt_distribution_agency', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_distribution_agency:importExcel', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 15:21:51', NULL, NULL, 0, 0, '1', 0);