import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { h, ref } from 'vue';
import { Tag } from 'ant-design-vue';
import { Image } from 'ant-design-vue';
import { useAgentStore } from '/@/store/modules/agent';
const agentStore = useAgentStore();
const agentList = ref();
agentList.value = agentStore.getAgent;
const jztApp = 'https://download.juzhitangapp.com?recommendBy=';
import { QrCode } from '/@/components/Qrcode/index';
import logoImg from '@/assets/logo.png';
// 生成的二维码文件
export const qrcodeUrl = ref('');
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '名称',
    align: "center",
    dataIndex: 'name'
  },
  {
    title: '手机号',
    align: "center",
    dataIndex: 'phone'
  },
  // {
  //   title: '上级代理',
  //   align: 'center',
  //   dataIndex: 'parent',
  // },
  // {
  //   title: '上级代理id',
  //   align: "center",
  //   dataIndex: 'pid'
  // },
  {
    title: '代理等级',
    align: "center",
    dataIndex: 'grade_dictText'
  },
  {
    title: '二维码地址',
    align: 'center',
    dataIndex: 'qrCode',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.qrCode,
        width: 40,
        height: 40,
      });
    },
  },
  // {
  //   title: '状态', // 01 待审核，02审核通过，03审核驳回
  //   align: 'center',
  //   dataIndex: 'status',
  //   customRender: ({ record }) => {
  //     return h(
  //       Tag,
  //       {
  //         color: getStatus(record.status, 'color'),
  //       },
  //       getStatus(record.status, 'name')
  //     );
  //   },
  // },
];

function getStatus(status, type) {
  if (status == '01') {
    return type === 'color' ? 'orange' : '待审核';
  }
  if (status == '02') {
    return type === 'color' ? 'blue' : '审核通过';
  }
  if (status == '03') {
    return type === 'color' ? 'red' : '审核驳回';
  }
}

//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: '名称',
    field: 'name',
    component: 'Input',
    colProps: { span: 5 },
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '名称',
    field: 'name',
    component: 'Input',
    dynamicRules: () => {
      return [{ required: true, message: '请输入名称!' }];
    },
  },
  {
    label: '用户Id',
    field: 'memberId',
    component: 'Input',
    // dynamicRules: () => {
    //   return [{ required: true, message: '请输入用户Id!' }];
    // },
    componentProps: {
      placeholder: '请在移动端我的页面下查看用户Id',
    },
  },
  {
    label: '手机号',
    field: 'phone',
    component: 'Input',
    dynamicRules: () => {
      return [{ required: true, message: '请输入手机号!' }];
    },
  },
  {
    label: '上级代理Id',
    field: 'pid',
    component: 'Input',
    componentProps: {
      placeholder: '请在当前页面下复制代理Id',
    },
    dynamicDisabled: ({values}) => {
      return values.isSub
    },
  },
  {
    label: '代理等级',
    field: 'grade',
    component: 'Select',
    componentProps: {
      options: agentList.value,
      //配置是否可搜索
      showSearch: true,
    },
  },
  {
    label: '二维码',
    field: 'qrCode',
    component: 'Input',
    render: (record) => {
      const code = ref();
      code.value = h(QrCode, { value: jztApp + record.values.memberId, logo: logoImg, onDone: onQrcodeDone });
      return record.values.memberId ? code.value : '';
    },
  },
  {
    label: '状态',
    field: 'isSub',
    component: 'Input',
    show: false,
  },
  // {
  //   label: '状态',
  //   field: 'status',
  //   component: 'Input',
  // },
  // TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];

function onQrcodeDone(data) {
  qrcodeUrl.value = data.url;
}

/**
 * 流程表单调用这个方法获取formSchema
 * @param param
 */
export function getBpmFormSchema(_formData): FormSchema[] {
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}