import { BasicColumn } from '/@/components/Table';
import { Image } from 'ant-design-vue';
import { h } from 'vue';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '用户id',
    align:"center",
    dataIndex: 'memberId'
   },
   {
    title: '姓名',
    align:"center",
    dataIndex: 'memberName'
   },
   {
    title: '手机号',
    align:"center",
    dataIndex: 'phone'
   },
   {
    title: '代理等级',
    align: "center",
    dataIndex: 'grade_dictText'
  },
  {
    title: '二维码地址',
    align: 'center',
    dataIndex: 'qrCode',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.qrCode,
        width: 40,
        height: 40,
      });
    },
  },
  {
    title: '操作',
    align: 'center',
    key: 'operation',
    width: 220,
    slots: { customRender: 'operation' },
  },
];
