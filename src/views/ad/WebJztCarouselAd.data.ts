import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { Image } from 'ant-design-vue';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import { h } from 'vue';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '展示图片',
    align: "center",
    dataIndex: 'image',
    customRender:({ record })=>{
      return h(Image,{
        src:record.image,
        width:135,
        height:47
      })
    }
  }
];

//查询数据
export const searchFormSchema: FormSchema[] = [
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '图片url',
    field: 'image',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
