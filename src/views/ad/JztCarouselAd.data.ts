import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { Image } from 'ant-design-vue';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import { h } from 'vue';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '展示图片',
    align: 'center',
    dataIndex: 'image',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.image,
        width: 135,
        height: 47,
      });
    },
  },
  {
    title: '地区',
    align: 'center',
    dataIndex: 'areaName',
  },
  {
    title: '年级',
    align: 'center',
    dataIndex: 'grade',
  },
  {
    title: '广告名称',
    align: 'center',
    dataIndex: 'name',
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
];

//表单数据
export const formSchema: FormSchema[] = [
  // {
  //   label: '图片url',
  //   field: 'image',
  //   component: 'Input',
  // },
  // {
  //   label: '是否删除：0否1是',
  //   field: 'isDel',
  //   component: 'InputNumber',
  // },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
