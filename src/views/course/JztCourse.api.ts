import { defHttp } from '/@/utils/http/axios';
import { useMessage } from '/@/hooks/web/useMessage';

const { createConfirm } = useMessage();

enum Api {
  list = '/web/webJztCourse/list',
  save = '/web/webJztCourse/add',
  edit = '/web/webJztCourse/edit',
  deleteOne = '/web/webJztCourse/delete',
  deleteBatch = '/web/webJztCourse/deleteBatch',
  importExcel = '/web/webJztCourse/importExcel',
  exportXls = '/web/webJztCourse/exportXls',
  importExcelId = '/web/webJztExamTopic/importExcel',
  importRealExcel = '/web/webJztExamPaperTopic/importExcel',
}

/**
 * 导出api
 * @param params
 */
export const getExportUrl = Api.exportXls;
/**
 * 导入真题api
 * **/
export const importExcelId = Api.importExcelId;
/**
 * 导入api
 */
export const getImportUrl = Api.importExcel;
/**
 * 导入真题
 */
export const getimportRealExcel = Api.importRealExcel;
/**
 * 列表接口
 * @param params
 */
export const list = (params) => defHttp.get({ url: Api.list, params });

/**
 * 删除单个
 * @param params
 * @param handleSuccess
 */
export const deleteOne = (params, handleSuccess) => {
  return defHttp.delete({ url: Api.deleteOne, params }, { joinParamsToUrl: true }).then(() => {
    handleSuccess();
  });
};

/**
 * 批量删除
 * @param params
 * @param handleSuccess
 */
export const batchDelete = (params, handleSuccess) => {
  createConfirm({
    iconType: 'warning',
    title: '确认删除',
    content: '是否删除选中数据',
    okText: '确认',
    cancelText: '取消',
    onOk: () => {
      return defHttp.delete({ url: Api.deleteBatch, data: params }, { joinParamsToUrl: true }).then(() => {
        handleSuccess();
      });
    },
  });
};

/**
 * 保存或者更新
 * @param params
 * @param isUpdate
 */
export const saveOrUpdate = (params, isUpdate) => {
  const url = isUpdate ? Api.edit : Api.save;
  return defHttp.post({ url: url, params }, { isTransformResponse: false });
};
