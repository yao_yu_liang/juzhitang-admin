import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import { h, ref } from 'vue';
import { Image, Switch } from 'ant-design-vue';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '课程表图片',
    align: "center",
    dataIndex: 'url',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.url,
        width: 100,
        height: 47
      })
    }
  },
  {
    title: '是否上线',
    align: "center",
    dataIndex: 'isOnline',
    customRender: ({ record }) => {
      return h(Switch, {
        checkedChildren: '是',
        unCheckedChildren: '否',
        checked: record.isOnline === '1' ? true : false,
      })
    }
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '课程表地址',
    field: 'url',
    component: 'Input',
  },
  {
    label: '是否上线',
    field: 'isOnline',
    component: 'Input',
  },
  {
    label: '月份',
    field: 'month',
    component: 'Input',
  },
  {
    label: '年级',
    field: 'grade',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
