-- 注意：该页面对应的前台目录为views/app文件夹下
-- 如果你想更改到其他目录，请修改sql中component字段对应的值


INSERT INTO sys_permission(id, parent_id, name, url, component, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_route, is_leaf, keep_alive, hidden, hide_tab, description, status, del_flag, rule_flag, create_by, create_time, update_by, update_time, internal_or_external) 
VALUES ('2024062509592050410', NULL, 'jzt_class_schedule', '/app/jztClassScheduleList', 'app/JztClassScheduleList', NULL, NULL, 0, NULL, '1', 0.00, 0, NULL, 1, 1, 0, 0, 0, NULL, '1', 0, 0, 'admin', '2024-06-25 09:59:41', NULL, NULL, 0);

-- 权限控制sql
-- 新增
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024062509592050411', '2024062509592050410', '添加jzt_class_schedule', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_class_schedule:add', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-25 09:59:41', NULL, NULL, 0, 0, '1', 0);
-- 编辑
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024062509592050412', '2024062509592050410', '编辑jzt_class_schedule', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_class_schedule:edit', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-25 09:59:41', NULL, NULL, 0, 0, '1', 0);
-- 删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024062509592050413', '2024062509592050410', '删除jzt_class_schedule', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_class_schedule:delete', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-25 09:59:41', NULL, NULL, 0, 0, '1', 0);
-- 批量删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024062509592050414', '2024062509592050410', '批量删除jzt_class_schedule', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_class_schedule:deleteBatch', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-25 09:59:41', NULL, NULL, 0, 0, '1', 0);
-- 导出excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024062509592050415', '2024062509592050410', '导出excel_jzt_class_schedule', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_class_schedule:exportXls', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-25 09:59:41', NULL, NULL, 0, 0, '1', 0);
-- 导入excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024062509592050416', '2024062509592050410', '导入excel_jzt_class_schedule', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_class_schedule:importExcel', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-25 09:59:41', NULL, NULL, 0, 0, '1', 0);