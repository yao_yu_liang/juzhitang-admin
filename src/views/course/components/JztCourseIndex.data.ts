import { BasicColumn } from '/@/components/Table';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '课程节点',
    align: 'center',
    dataIndex: 'name',
  },
  {
    title: '节点名称',
    align: 'center',
    dataIndex: 'name',
  },
  {
    title: '开课时间',
    align: 'center',
    dataIndex: 'startTime',
  },
  {
    title: '节点简介',
    align: 'center',
    dataIndex: 'note',
  },
  {
    title: '直播推流地址',
    align: 'center',
    dataIndex: 'liveUrl',
  },
  {
    title: '操作',
    align: 'center',
    key: 'operation',
    width: 220,
    slots: { customRender: 'operation' },
  },
];
