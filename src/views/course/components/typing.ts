import type Cropper from 'cropperjs';

export interface CropendResult {
  imgBase64: string;
  imgInfo: Cropper.Data;
}

export interface GradeType {
  value: string;
  text: string;
  title: string;
  label: string;
}

export interface SubjectType {
  value: string;
  text: string;
  title: string;
  label: string;
}

export interface FileItem {
  uid: string;
  name?: string;
  status?: string;
  response?: string;
  url?: string;
}

export interface FileInfo {
  file: FileItem;
  fileList: FileItem[];
}

export type { Cropper };
