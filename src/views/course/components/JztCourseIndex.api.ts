import { defHttp } from '/@/utils/http/axios';

enum Api {
  courseLiverUrl = '/web/webJztCourseNode/liveUrl',
  indexAdd = '/web/webJztCourseNode/add',
  indexEdit = '/web/webJztCourseNode/edit',
  indexDel = '/web/webJztCourseNode/delete',
}

/**
 * 生成直播地址
 * @param params
 */
export const getCourseLiverUrl = (params) => {
  return defHttp.get({ url: Api.courseLiverUrl, params }, { successMessageMode: 'none' });
};

/**
 * 课程节点新增 / 修改
 * @param params
 */
export const saveOrUpdate = (params, isUpdate) => {
  const url = isUpdate ? Api.indexEdit : Api.indexAdd;
  return defHttp.post({ url: url, params }, { isTransformResponse: false });
};

/**
 * 删除
 * @param params
 */
export const indexDel = (params) => {
  return defHttp.delete({ url: Api.indexDel + '?id=' + params.id });
};
