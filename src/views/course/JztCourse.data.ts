import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h, ref } from 'vue';
import { Image } from 'ant-design-vue';
import { useCourseTypeStore } from '/@/store/modules/course';

const courseStore = useCourseTypeStore();
const courseTypeOption = ref();
const courseGradeOption = ref();
courseTypeOption.value = courseStore.getCourseType;
courseGradeOption.value = courseStore.getCourseGrade;
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '课程名称',
    align: 'center',
    dataIndex: 'name',
  },
  {
    title: '课程图片',
    align: 'center',
    dataIndex: 'image',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.image,
        width: 75,
        height: 43,
      });
    },
  },
  {
    title: '展示图片',
    align: 'center',
    dataIndex: 'imageHome',
    customRender: ({ record }) => {
      return h(Image, {
        src: record.imageHome,
        width: 34,
        height: 40,
      });
    },
  },
  {
    title: '课程概述/简介',
    align: 'center',
    dataIndex: 'courseOverview',
  },
  {
    title: '价格',
    align: 'center',
    dataIndex: 'price',
  },
  {
    title: '备注',
    align: 'center',
    dataIndex: 'note',
  },
  {
    title: '是否推荐',
    align: 'center',
    dataIndex: 'isRecommend_dictText',
  },
  {
    title: '年级',
    align: 'center',
    dataIndex: 'grade_dictText',
  },
  {
    title: '地区',
    align: 'center',
    dataIndex: 'areaId',
  },
  {
    title: '学科',
    align: 'center',
    dataIndex: 'subjectId_dictText',
  },
  {
    title: '标签',
    align: 'center',
    dataIndex: 'tags',
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: '课程名称',
    field: 'name',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    field: 'subjectId',
    label: '分类',
    component: 'Select',
    componentProps: {
      options: courseTypeOption.value,
    },
    colProps: { span: 5 },
  },
  {
    field: 'grade',
    label: '年级',
    component: 'Select',
    componentProps: {
      options: courseGradeOption.value,
    },
    colProps: { span: 5 },
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '课程名称',
    field: 'name',
    component: 'Input',
    dynamicRules: () => {
      return [{ required: true, message: '请输入课程名称!' }];
    },
  },
  {
    label: '课程图片',
    field: 'image',
    component: 'Input',
  },
  {
    label: '课程概述/简介',
    field: 'courseOverview',
    component: 'InputTextArea',
  },
  {
    label: '价格',
    field: 'price',
    component: 'InputNumber',
  },
  {
    label: '备注',
    field: 'note',
    component: 'Input',
  },
  {
    label: '是否推荐',
    field: 'isRecommend',
    component: 'InputNumber',
  },
  {
    label: '年级',
    field: 'grade',
    component: 'Input',
  },
  {
    label: '地区',
    field: 'areaId',
    component: 'Input',
  },
  {
    label: '学科',
    field: 'subjectId',
    component: 'Input',
  },
  {
    label: '标签',
    field: 'tags',
    component: 'Input',
  },
  // TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
