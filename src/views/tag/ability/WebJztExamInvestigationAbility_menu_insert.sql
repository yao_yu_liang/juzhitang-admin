-- 注意：该页面对应的前台目录为views/web文件夹下
-- 如果你想更改到其他目录，请修改sql中component字段对应的值


INSERT INTO sys_permission(id, parent_id, name, url, component, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_route, is_leaf, keep_alive, hidden, hide_tab, description, status, del_flag, rule_flag, create_by, create_time, update_by, update_time, internal_or_external) 
VALUES ('2024061210229570080', NULL, 'jzt_exam_investigation_ability', '/web/webJztExamInvestigationAbilityList', 'web/WebJztExamInvestigationAbilityList', NULL, NULL, 0, NULL, '1', 0.00, 0, NULL, 1, 1, 0, 0, 0, NULL, '1', 0, 0, 'admin', '2024-06-12 10:22:08', NULL, NULL, 0);

-- 权限控制sql
-- 新增
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061210229580081', '2024061210229570080', '添加jzt_exam_investigation_ability', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_exam_investigation_ability:add', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 10:22:08', NULL, NULL, 0, 0, '1', 0);
-- 编辑
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061210229580082', '2024061210229570080', '编辑jzt_exam_investigation_ability', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_exam_investigation_ability:edit', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 10:22:08', NULL, NULL, 0, 0, '1', 0);
-- 删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061210229580083', '2024061210229570080', '删除jzt_exam_investigation_ability', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_exam_investigation_ability:delete', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 10:22:08', NULL, NULL, 0, 0, '1', 0);
-- 批量删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061210229580084', '2024061210229570080', '批量删除jzt_exam_investigation_ability', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_exam_investigation_ability:deleteBatch', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 10:22:08', NULL, NULL, 0, 0, '1', 0);
-- 导出excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061210229580085', '2024061210229570080', '导出excel_jzt_exam_investigation_ability', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_exam_investigation_ability:exportXls', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 10:22:08', NULL, NULL, 0, 0, '1', 0);
-- 导入excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2024061210229580086', '2024061210229570080', '导入excel_jzt_exam_investigation_ability', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:jzt_exam_investigation_ability:importExcel', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2024-06-12 10:22:08', NULL, NULL, 0, 0, '1', 0);