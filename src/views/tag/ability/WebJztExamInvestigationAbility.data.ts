import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';

//列表数据
export const columns: BasicColumn[] = [
  {
    title: '学科',
    align: "center",
    dataIndex: 'subjectName'
  },
  {
    title: '考察能力',
    align: "center",
    dataIndex: 'name'
  },
  {
    title: '考察能力描述',
    align: "center",
    dataIndex: 'description'
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '学科id',
    field: 'subjectId',
    component: 'InputNumber',
  },
  {
    label: '考察能力',
    field: 'name',
    component: 'Input',
  },
  {
    label: '考察能力描述',
    field: 'description',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
