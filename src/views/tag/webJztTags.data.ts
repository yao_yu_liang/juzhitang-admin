import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '上级标签名称',
    align: "center",
    dataIndex: 'parentName'
  },
  {
    title: '学科名称',
    align: "center",
    dataIndex: 'subjectName'
  },
  {
    title: '标签名称',
    align: "center",
    dataIndex: 'tagName'
  },
  {
    title: '标签层级',
    align: "center",
    dataIndex: 'grade'
  },
];
columns.forEach((ele)=>{
  if(ele.dataIndex === 'grade'){
    ele.customRender = ({ text }) =>{
      let content = ''
      switch(text){
        case '01': content = '一级'
        break
        case '02': content = '二级'
        break
        case '03': content = '三级'
        break
        case '04': content = '四级'
        break
        case '05': content = '五级'
        break
      }
      return content  
    }
  }
})

//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: '学科',
    field: 'subjectName',
    component: 'Input',
  },
  {
    label: '层级',
    field: 'grade',
    component: 'Input',
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '上级标签名称',
    field: 'parentName',
    component: 'InputNumber',
  },
  {
    label: '学科名称',
    field: 'subjectName',
    component: 'Input',
  },
  {
    label: '标签名称',
    field: 'tagName',
    component: 'Input',
  },
  {
    label: '标签层级',
    field: 'grade',
    component: 'Select',
    componentProps: {
      options: [
        {value:'01',label:'一级'},
        {value:'02',label:'二级'},
        {value:'03',label:'三级'},
        {value:'04',label:'四级'},
        {value:'05',label:'五级'},
      ],
    },
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
