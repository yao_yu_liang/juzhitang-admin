import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { h, ref } from 'vue';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
   {
    title: '用户名称',
    align:"center",
    dataIndex: 'memberName'
   },
   {
    title: '课程名称',
    align:"center",
    dataIndex: 'courseName'
   },
   {
    title: '课程节点名称',
    align:"center",
    dataIndex: 'courseNodeName'
   },
   {
    title: '价格',
    align:"center",
    dataIndex: 'price'
   },
   {
    title: '商品编号',
    align:"center",
    dataIndex: 'orderNo'
   },
   {
    title: '商品名称',
    align:"center",
    dataIndex: 'description'
   },
   {
    title: 'ip',
    align:"center",
    dataIndex: 'ip'
   },
   {
    title: '订单状态',
    align:"center",
    dataIndex: 'orderStatus',
   },
   {
    title: '消费类型',
    align:"center",
    dataIndex: 'productType',
   },

];
columns.forEach((ele)=>{
  if(ele.dataIndex === 'productType'){
    ele.customRender = ({ text }) =>{
      let content = ''
      switch(text){
        case '01': content = '购买课程'
        break
        case '02': content = '充值'
        break
        case '03': content = '购买套餐'
        break
        case '04': content = '积分商城'
        break
      }
      return content  
    }
  }
})
//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: '用户名称',
    field: 'memberName',
    component: 'Input',
    colProps: { span: 5 },
  },
  {
    label: '课程名称',
    field: 'courseName',
    component: 'Input',
    colProps: { span: 5 },
  },
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: 'memberId',
    field: 'memberId',
    component: 'Input',
  },
  {
    label: 'courseId',
    field: 'courseId',
    component: 'Input',
  },
  {
    label: '创建人',
    field: 'createdBy',
    component: 'Input',
  },
  {
    label: '创建时间',
    field: 'createdTime',
    component: 'DatePicker',
  },
  {
    label: '更新人',
    field: 'updatedBy',
    component: 'Input',
  },
  {
    label: '更新时间',
    field: 'updatedTime',
    component: 'DatePicker',
  },
  {
    label: '价格',
    field: 'price',
    component: 'InputNumber',
  },
  {
    label: '支付方式（杉德 充值）',
    field: 'type',
    component: 'Input',
  },
  {
    label: '是否过期',
    field: 'isExpire',
    component: 'Input',
  },
  {
    label: '截至时间',
    field: 'endTime',
    component: 'DatePicker',
  },
  {
    label: '商品编号',
    field: 'orderNo',
    component: 'Input',
  },
  {
    label: '商品描述',
    field: 'description',
    component: 'Input',
  },
  {
    label: 'ip',
    field: 'ip',
    component: 'Input',
  },
  {
    label: '订单状态',
    field: 'status',
    component: 'Input',
  },
  {
    label: '订单图片',
    field: 'image',
    component: 'Input',
  },
  {
    label: '产品类型 01 购买课程 02 充值 03 购买套餐 04 积分商城 ',
    field: 'producttype',
    component: 'Input',
  },
  {
    label: '市场产品 ',
    field: 'marketproduct',
    component: 'Input',
  },
  {
    label: '支付金额',
    field: 'amount',
    component: 'InputNumber',
  },
  {
    label: '付费模式',
    field: 'paymode',
    component: 'Input',
  },
  {
    label: '费用金额',
    field: 'feeamt',
    component: 'InputNumber',
  },
  {
    label: '订单状态',
    field: 'orderstatus',
    component: 'Input',
  },
  {
    label: '杉德序列号',
    field: 'sandserialno',
    component: 'Input',
  },
  {
    label: '渠道订单号',
    field: 'channelorderno',
    component: 'Input',
  },
  {
    label: '出单号（项目定义）',
    field: 'outorderno',
    component: 'Input',
  },
  {
    label: '支付类型 01 支付宝 02 微信 03 钱包',
    field: 'paytype',
    component: 'Input',
  },
  {
    label: '完成时间',
    field: 'finishedtime',
    component: 'DatePicker',
  },
  {
    label: '课程id',
    field: 'courseNodeId',
    component: 'Input',
  },
  {
    label: 'prizeId',
    field: 'prizeId',
    component: 'Input',
  },
  {
    label: 'skuId',
    field: 'skuId',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];



/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}