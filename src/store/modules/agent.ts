import { defineStore } from 'pinia';
import { getGradeList } from '/@/api/agent/agent';

interface AgentListType {
  value: String;
  label: String;
}

export const useAgentStore = defineStore({
  id: 'agent-store',
  state: () => ({
    agentListOption: [],
  }),
  getters: {
    getAgent(): AgentListType[] {
      return this.agentListOption;
    },
  },
  actions: {
    async getAgentListOptionList() {
      this.agentListOption = await getGradeList();
      console.log(this.agentListOption);
    },
  },
});
