import { defineStore } from 'pinia';
import { tagsList, subjectList } from '@/api/topic/index';

interface tagsType {
  tagName: String;
  id: String;
}

export const useTopicStore = defineStore({
  id: 'topic-type',
  state: () => ({
    topicTagsList: [],
    subjectListOptions: [],
  }),
  getters: {
    getTopicTagsList(): tagsType[] {
      return this.topicTagsList;
    },
    getSubjectList(): any[] {
      return this.subjectListOptions;
    },
  },
  actions: {
    async getTagsList(params = {}) {
      this.topicTagsList = await tagsList(params);
    },
    async subjectList(params = {}) {
      this.subjectListOptions = await subjectList(params);
    },
  },
});
