import { defineStore } from 'pinia';
import { category } from '@/views/mall/product/WebJztProduct.api';

interface MallType {
  value: String;
  label: String;
}

export const useMallTypeStore = defineStore({
  id: 'mall-type',
  state: () => ({
    categoryOption: [],
  }),
  getters: {
    getCategory(): MallType[] {
      this.getCategoryList();
      return this.categoryOption;
    },
  },
  actions: {
    async getCategoryList() {
      const categorys = await category({});
      categorys.map((item) => {
        this.categoryOption.push({value: item.code, label: item.text});
      });
    },
  },
});
