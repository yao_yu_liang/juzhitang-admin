import { defineStore } from 'pinia';
import { getSubjectList, getGradeList, getContentTypeList, getAreaList, getTextbookList } from '@/api/course/index';

interface CourseType {
  value: String;
  label: String;
}

export const useCourseTypeStore = defineStore({
  id: 'course-type',
  state: () => ({
    courseTypeOption: [],
    courseGradeOption: [],
    courseContentTypeOption: [],
    areaOption: [],
    textbookOption: [],
  }),
  getters: {
    getCourseType(): CourseType[] {
      return this.courseTypeOption;
    },
    getCourseGrade(): CourseType[] {
      return this.courseGradeOption;
    },
    getCourseContentType(): CourseType[] {
      return this.courseContentTypeOption;
    },
    getArea(): CourseType[] {
      return this.areaOption;
    },
    getTextBook(): CourseType[] {
      return this.textbookOption;
    },
  },
  actions: {
    async getCourseTypeList() {
      this.courseTypeOption = await getSubjectList({});
    },
    async getCourseGradeList() {
      this.courseGradeOption = await getGradeList({});
    },
    async getContentTypeList() {
      this.courseContentTypeOption = await getContentTypeList({});
    },
    async getAreaList() {
      this.areaOption = await getAreaList({});
    },
    async getTextBookList() {
      this.textbookOption = await getTextbookList({});
    },
  },
});
