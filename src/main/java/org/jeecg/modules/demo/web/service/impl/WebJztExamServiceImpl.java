package org.jeecg.modules.demo.web.service.impl;

import org.jeecg.modules.demo.web.entity.WebJztExam;
import org.jeecg.modules.demo.web.mapper.WebJztExamMapper;
import org.jeecg.modules.demo.web.service.IWebJztExamService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: jzt_exam
 * @Author: jeecg-boot
 * @Date:   2024-04-17
 * @Version: V1.0
 */
@Service
public class WebJztExamServiceImpl extends ServiceImpl<WebJztExamMapper, WebJztExam> implements IWebJztExamService {

}
