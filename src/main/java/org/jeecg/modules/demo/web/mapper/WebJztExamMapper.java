package org.jeecg.modules.demo.web.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.web.entity.WebJztExam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: jzt_exam
 * @Author: jeecg-boot
 * @Date:   2024-04-17
 * @Version: V1.0
 */
public interface WebJztExamMapper extends BaseMapper<WebJztExam> {

}
