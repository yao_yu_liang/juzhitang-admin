package org.jeecg.modules.demo.web.service;

import org.jeecg.modules.demo.web.entity.WebJztExam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: jzt_exam
 * @Author: jeecg-boot
 * @Date:   2024-04-17
 * @Version: V1.0
 */
public interface IWebJztExamService extends IService<WebJztExam> {

}
