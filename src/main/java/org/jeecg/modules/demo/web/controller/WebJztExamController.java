package org.jeecg.modules.demo.web.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.web.entity.WebJztExam;
import org.jeecg.modules.demo.web.service.IWebJztExamService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: jzt_exam
 * @Author: jeecg-boot
 * @Date:   2024-04-17
 * @Version: V1.0
 */
@Api(tags="jzt_exam")
@RestController
@RequestMapping("/web/webJztExam")
@Slf4j
public class WebJztExamController extends JeecgController<WebJztExam, IWebJztExamService> {
	@Autowired
	private IWebJztExamService webJztExamService;
	
	/**
	 * 分页列表查询
	 *
	 * @param webJztExam
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "jzt_exam-分页列表查询")
	@ApiOperation(value="jzt_exam-分页列表查询", notes="jzt_exam-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<WebJztExam>> queryPageList(WebJztExam webJztExam,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WebJztExam> queryWrapper = QueryGenerator.initQueryWrapper(webJztExam, req.getParameterMap());
		Page<WebJztExam> page = new Page<WebJztExam>(pageNo, pageSize);
		IPage<WebJztExam> pageList = webJztExamService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param webJztExam
	 * @return
	 */
	@AutoLog(value = "jzt_exam-添加")
	@ApiOperation(value="jzt_exam-添加", notes="jzt_exam-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:jzt_exam:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody WebJztExam webJztExam) {
		webJztExamService.save(webJztExam);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param webJztExam
	 * @return
	 */
	@AutoLog(value = "jzt_exam-编辑")
	@ApiOperation(value="jzt_exam-编辑", notes="jzt_exam-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:jzt_exam:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody WebJztExam webJztExam) {
		webJztExamService.updateById(webJztExam);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "jzt_exam-通过id删除")
	@ApiOperation(value="jzt_exam-通过id删除", notes="jzt_exam-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:jzt_exam:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		webJztExamService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "jzt_exam-批量删除")
	@ApiOperation(value="jzt_exam-批量删除", notes="jzt_exam-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:jzt_exam:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.webJztExamService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "jzt_exam-通过id查询")
	@ApiOperation(value="jzt_exam-通过id查询", notes="jzt_exam-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<WebJztExam> queryById(@RequestParam(name="id",required=true) String id) {
		WebJztExam webJztExam = webJztExamService.getById(id);
		if(webJztExam==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(webJztExam);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param webJztExam
    */
    //@RequiresPermissions("org.jeecg.modules.demo:jzt_exam:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WebJztExam webJztExam) {
        return super.exportXls(request, webJztExam, WebJztExam.class, "jzt_exam");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("jzt_exam:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WebJztExam.class);
    }

}
