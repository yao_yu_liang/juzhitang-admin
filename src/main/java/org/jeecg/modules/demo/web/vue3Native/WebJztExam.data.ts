import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '标题',
    align: "center",
    dataIndex: 'title'
  },
  {
    title: '年级',
    align: "center",
    dataIndex: 'grade'
  },
  {
    title: '学科',
    align: "center",
    dataIndex: 'subjectId'
  },
  {
    title: '地区',
    align: "center",
    dataIndex: 'areaId'
  },
  {
    title: '地址',
    align: "center",
    dataIndex: 'ossUrl'
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '标题',
    field: 'title',
    component: 'Input',
  },
  {
    label: '年级',
    field: 'grade',
    component: 'Input',
  },
  {
    label: '学科',
    field: 'subjectId',
    component: 'Input',
  },
  {
    label: '地区',
    field: 'areaId',
    component: 'Input',
  },
  {
    label: '地址',
    field: 'ossUrl',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
